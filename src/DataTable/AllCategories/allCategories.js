import { Button, Popover } from 'antd';
import More from '../../asset/more.svg'
import {ReactComponent as History} from '../../asset/history.svg';
import {ReactComponent as Delete} from '../../asset/delete.svg';

const columns = [
    {
      title: 'Category',
      dataIndex: 'category',
      key: 'category',
      render: text => <a href='# ' style={{fontSize:'12px', fontWeight:600, color:'#00AB6B'}}>{text}</a>,
    },
    {
      title: 'Feature',
      dataIndex: 'feature',
      key: 'feature',
    },
    {
      title: 'Sub-Feature',
      dataIndex: 'subfeature',
      key: 'subfeature',
    },
    {
      title: 'Project Manager',
      dataIndex: 'projectmanager',
      key: 'projectmanager',
    },
    {
      title: 'Partners',
      dataIndex: 'partner',
      key: 'partner', 
    },
    {
      title: 'Link to Figma',
      key: 'link',
      dataIndex: 'link',
      align: 'center',
      render: (tags) => ( 
        <Button className="figma-link-tag">
          <a href={tags} target="_blank">Click Here</a>
        </Button>
       
      ),
    },
    {
      title: 'Action',
      dataIndex: 'action',
      key: 'action',
      render: () =>
      <Popover className='action-btn' placement="leftBottom" content={this.props.content} trigger="click">
        <Button><img src={More} alt=""/></Button>
      </Popover>
    },
  ];
export default columns;
