import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import reducers from '../redux-saga/reducers'
import rootSaga from '../redux-saga/sagas'


const sagaMiddleware = createSagaMiddleware()

const store = createStore(
   combineReducers({
      ...reducers,
   }),
   window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
   compose(applyMiddleware(sagaMiddleware))
)

sagaMiddleware.run(rootSaga)

export {store}