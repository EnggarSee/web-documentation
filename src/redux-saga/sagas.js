import { all } from 'redux-saga/effects'

import AllCategoriesSaga from '../redux/saga'

export default function * useSaga () {
   yield all([
      AllCategoriesSaga()
   ])
 }