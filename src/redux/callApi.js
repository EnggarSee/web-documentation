import axios from 'axios'

const getViewData = async(payload) => {
   try{
      const url = 'https://5e9fca5511b078001679cd41.mockapi.io/webdoc'
      return await axios.get(url)
   }catch(error){
      return error.messages
   }
}

const addNewTask = async(payload) => {
   console.log(payload.payload.data, 'PAYLOAD API');
   try{
      const url = 'https://5e9fca5511b078001679cd41.mockapi.io/webdoc'
      return await axios.post(url, payload.payload.data)
   }catch(error){
      return error.messages
   }
}

const deleteViewData = async(payload) => {
   console.log(payload, 'DELETEEE');
   try{
      return await axios.delete(`https://5e9fca5511b078001679cd41.mockapi.io/webdoc/${payload.payload}`)
   }catch(error){
      return error.messages
   }
}

const getDetailData = async(payload) => {
   console.log(payload, 'CALL API EDTAIL');
   try{
      return await axios.get(`https://5e9fca5511b078001679cd41.mockapi.io/webdoc/${payload.payload}`)
   }catch(error){
      return error.message
   }
}

const editDetailData = async(payload) => {
   console.log(payload, 'CALL API EDIT');
   try{
      return await axios.put(`https://5e9fca5511b078001679cd41.mockapi.io/webdoc/${payload.payload.id}`, payload.payload.editData)
   }catch(error){
      return error.message
   }
}

const exportCallApi ={
    getViewData,
    addNewTask,
    deleteViewData,
    getDetailData,
    editDetailData,
}

export default exportCallApi