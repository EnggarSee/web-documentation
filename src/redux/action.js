const actions = {
    GET_DATA_LIST : 'GET_DATA_LIST',
    DATA_LIST : 'DATA_LIST',

    ADD_NEW_TASK : 'ADD_NEW_TASK',
    NEW_TASK : 'NEW_TASK',

    DELETE_DATA_LIST : 'DELETE_DATA_LIST',
    DATA_LIST_DELETE : 'DATA_LIST_DELETE',

    GET_DETAIL_DATA : 'GET_DETAIL_DATA',
    DETAIL_DATA : 'DETAIL_DATA',

    EDIT_DETAIL_DATA : 'EDIT_DETAIL_DATA',

    getDataList : payload => ({
        type : actions.GET_DATA_LIST,
        payload
     }),

    addNewTask : payload => ({
        type: actions.ADD_NEW_TASK,
        payload
    }),

    deleteDatalist : payload => ({
        type : actions.DELETE_DATA_LIST,
        payload
    }),

    getDetailData : payload => ({
        type : actions.GET_DETAIL_DATA,
        payload
    }),

    editDetailData : payload => ({
        type: actions.EDIT_DETAIL_DATA,
        payload
    })
}

export default actions