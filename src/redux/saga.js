import { all, takeEvery, put, fork, call } from 'redux-saga/effects'
import actions from './action'
import callApi from './callApi'

export function * getViewData () {
    yield takeEvery('GET_DATA_LIST', function * (payload) {
      try {
        let res = yield call(callApi.getViewData, payload)
        if (res.status === 200 || res.status === 201) {
          console.log(res, 'PAYLOAD SAGAAA');
          yield put({
            type: actions.DATA_LIST,
            data: res.data,
            message: 'success'
          })
        } else {
          yield put({ type: actions.DATA_LIST, data: [res.data], message: 'failed' })
        }
      } catch (error) {
        yield put({ type: actions.DATA_LIST, data: [] })
      }
    })
  }

export function * getDetailData () {
    yield takeEvery('GET_DETAIL_DATA', function * (payload) {
      console.log(payload, 'DETAIL PAYLOAD');
      try {
        let res = yield call(callApi.getDetailData, payload)
        if (res.status === 200 || res.status === 201) {
          console.log(res, 'PAYLOAD DETAIL');
          yield put({
            type: actions.DETAIL_DATA,
            data: res.data,
            message: 'success'
          })
        } else {
          yield put({ type: actions.DETAIL_DATA, data: [res.data], message: 'failed' })
        }
      } catch (error) {
        yield put({ type: actions.DETAIL_DATA, data: [] })
      }
    })
  }

export function * addNewTask () {
  console.log('SAGAAA');
  yield takeEvery('ADD_NEW_TASK', function * (payload) {
    try {
      let res = yield call(callApi.addNewTask, payload)
      console.log(res,' RES SAGA');
      if (res.status === 200 || res.status === 201) {
        console.log(res.data, 'PAYLOAD SAGA');
        yield put({
          type: actions.GET_DATA_LIST,
          data: res.data,
          message: 'success'
        })
      } else {
        yield put({ type: actions.GET_DATA_LIST, data: [res.data], message: 'failed' })
      }
    } catch (error) {
      yield put({ type: actions.GET_DATA_LIST, data: [] })
    }
  })
}

export function * deleteViewData () {
  yield takeEvery('DELETE_DATA_LIST', function * (payload) {
      console.log(payload, 'DELETE PAYLOAD');
    try {
      let res = yield call(callApi.deleteViewData, payload)
      if (res.status === 200 || res.status === 201) {
        console.log(res.data, 'PAYLOAD DELETE');
        yield put({
          type: actions.GET_DATA_LIST,
          data: res.data,
          message: 'success'
        })
      } else {
        yield put({ type: actions.GET_DATA_LIST, data: [res.data], message: 'failed' })
      }
    } catch (error) {
      yield put({ type: actions.GET_DATA_LIST, data: [] })
    }
  })
 }

 export function * editDetailData () {
  yield takeEvery('EDIT_DETAIL_DATA', function * (payload) {
      console.log(payload, 'EDIT PAYLOAD');
    try {
      let res = yield call(callApi.editDetailData, payload)
      console.log(res, 'EDIT RESSSSSS');
      if (res.status === 200 || res.status === 201) {
        console.log(res.data, 'PAYLOAD EDITTT');
        yield put({
          type: actions.GET_DATA_LIST,
          data: res.data,
          message: 'success'
        })
      } else {
        yield put({ type: actions.GET_DATA_LIST, data: [res.data], message: 'failed' })
      }
    } catch (error) {
      yield put({ type: actions.GET_DATA_LIST, data: [] })
    }
  })
 }


export default function * rootSaga () {
    yield all([
     fork(getViewData),
     fork(addNewTask),
     fork(deleteViewData),
     fork(getDetailData),
     fork(editDetailData)
    ])
  }