import actions from './action'

const initialState = {
    AllCategoriesList : '',
    loader: false,
    addDataMessage: '',
    dataView: '',
    newTask: {},
    detailData: {
        category:'',
        feature:'',
        link:'',
        partner:'',
        projectmanager:'',
        subfeature:'',
    },
    loaderDetail: false,
}

export default function ViewsReducer (state = initialState, action){
    switch (action.type){
        case actions.GET_DATA_LIST:
            return{
                ...state,
                AllCategoriesList: '',
                loader: true
            }
        case actions.DATA_LIST:
            return{
                ...state,
                AllCategoriesList: action.data,
                loader: false
            }
        case actions.ADD_NEW_TASK:
            return{
                ...state,
                loader: true
            }
        case actions.NEW_TASK:
            return{
                ...state,
                newTask: action.data,
                loader: false
            }
        case actions.DELETE_DATA_LIST:
            return{
                ...state,
                loader: true
            }
        case actions.DATA_LIST_DELETE:
            return{
                ...state,
                newTask: action.data,
                loader: false,
            }
        case actions.GET_DETAIL_DATA:
            return{
                ...state,
                loaderDetail: true,
            }
        case actions.DETAIL_DATA:
            return{
                ...state,
                detailData: action.data,
                newTask: action.data,
                loaderDetail: false,
            }
        case actions.EDIT_DETAIL_DATA:
            return{
                ...state,
                loader: true,
            }
        default:
            return state
    }
}