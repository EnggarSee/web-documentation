import React, { Fragment } from 'react'
import '../AllCategories/AllCategories.css'
import { Input, Button, Table, Modal, Form, Select, Spin, Popover } from 'antd';
import {ReactComponent as Search} from '../../asset/search.svg'
import {ReactComponent as Plus} from '../../asset/add.svg'
import { connect } from 'react-redux'
import action from '../../redux/action'
import More from '../../asset/more.svg'
import {ReactComponent as History} from '../../asset/history.svg';
import {ReactComponent as Delete} from '../../asset/delete.svg';

const { getDataList, addNewTask, deleteDatalist, getDetailData, editDetailData } = action

class AllCategories extends React.Component{
    constructor (props){
        super(props)
        this.state = {
            dataAdd : '',
            isModalVisible: false,
            newTask : {
                link: '',
                notes: '',
                tags: '',
            },
            idDelete: '',
            editable: false,
            detailData: {},
        }
    }

    static getDerivedStateFromProps (nextProps, prevState) {

        
        if (nextProps.AllCategoriesState.AllCategoriesList !== prevState.AllCategoriesList || nextProps.detailDataState !== prevState.detailData){
            return{
                data : nextProps.AllCategoriesState.AllCategoriesList,
                loader: nextProps.AllCategoriesState.loader,
                loaderDetail : nextProps.AllCategoriesState.loaderDetail,
                detailData : nextProps.AllCategoriesState.detailData
            }
            
        }

    
       
    }

    componentDidMount() {
        this.props.getDataList()
     }

    componentDidUpdate(prevProps, prevState){
        const{ detailData } = this.state
        console.log(prevProps.AllCategoriesState, 'prevv');
        console.log(this.state.detailData, 'PREVSTAE');
        if(prevProps.AllCategoriesState.detailData !== detailData){
            console.log("masuk");
            this.setState({
                newTask: {
                    category: detailData.category,
                    feature: detailData.feature,
                    link: detailData.link,
                    partner: detailData.partner,
                    projectmanager: detailData.projectmanager,
                    subfeature: detailData.subfeature,
                }, 
            })
        }
    }

    addNewTast () {
        this.setState({
            isModalVisible : true,
        })
    };

    handleCancel = () =>{
        this.setState({
           isModalVisible : false
        })
     }
    addNewData (value, key) {
        this.setState(prevState => ({
            newTask: {
                ...prevState.newTask,
                [key] : value,
            }
        }))
    }

    addNewDataInput (value, key) {
        this.setState(prevState => ({
            newTask: {
                ...prevState.newTask,
                [key] : value.target.value,
            }
        }))
    }

    addNewTask () {
        const {newTask} = this.state
        this.props.addNewTask({
            data: newTask
        })
        this.setState({
            isModalVisible : false,
            newTask: {
                link : '',
                notes: '',
                tags: '',
            },
            
        }) 
    }

    editDetailData () {
       const { idDelete, newTask, detailData } = this.state 
        console.log(newTask, 'newTaskkk');
        this.setState(prevState =>({
            isModalVisible : false,
            newTask: {
                category: detailData.category,
                feature: detailData.feature,
                link: detailData.link,
                partner: detailData.partner,
                projectmanager: detailData.projectmanager,
                subfeature: detailData.subfeature,
            }, 
        
        })) 
        this.props.editDetailData({ 
            id: idDelete,
            editData: newTask,
         })
    }

    deleteTask () {
        this.props.deleteDatalist(this.state.idDelete)
    }

    editDetail () {
        this.props.getDetailData(this.state.idDelete)
        const { detailData } = this.state
        this.setState({
            isModalVisible: true,
            editable: true, 
            newTask: {
                category: detailData.category,
                feature: detailData.feature,
                link: detailData.link,
                partner: detailData.partner,
                projectmanager: detailData.projectmanager,
                subfeature: detailData.subfeature,
            }, 
        })
    }

    cancelModal () {
        this.setState({
            isModalVisible : false,
        })
        window.location.reload()
    }

    

    render(){
        const {isModalVisible, data, loader , newTask, loaderDetail, detailData, editable} = this.state

        const content = (
            <div className='pop-over-wrapper'>
              <div className='content' onClick={() => this.editDetail()}>
                <History />
                <p>History</p>
              </div>
              <div className='content' onClick={() => this.deleteTask()}>
                <Delete />
                <p>Edit</p>
              </div>
            </div>
        );

        const columns = [
            {
              title: 'Category',
              dataIndex: 'category',
              key: 'category',
              render: text => <a href='# ' style={{fontSize:'12px', fontWeight:600, color:'#00AB6B'}}>{text}</a>,
            },
            {
              title: 'Feature',
              dataIndex: 'feature',
              key: 'feature',
            },
            {
              title: 'Sub-Feature',
              dataIndex: 'subfeature',
              key: 'subfeature',
            },
            {
              title: 'Project Manager',
              dataIndex: 'projectmanager',
              key: 'projectmanager',
            },
            {
              title: 'Partners',
              dataIndex: 'partner',
              key: 'partner', 
            },
            {
              title: 'Link to Figma',
              key: 'link',
              dataIndex: 'link',
              align: 'center',
              render: (tags) => ( 
                <Button className="figma-link-tag">
                  <a href={tags} target="_blank" rel="noreferrer">Click Here</a>
                </Button>
               
              ),
            },
            {
              title: 'Action',
              dataIndex: 'action',
              key: 'action',
              render: (val, data) =>
              <Popover className='action-btn' placement="leftBottom" content={loader === true ? false : isModalVisible ? false : content}  trigger="click">
                <Button onClick={()=> this.setState({idDelete : data.id})}><img src={More} alt=""/></Button>
              </Popover>
            },
          ];
       
        return(
            <Fragment>
                <Spin spinning={loader}>
                <div className='all-categories-page'>
                    <div className='header-wrapper'>
                        <div className='left-content'>
                            <p>All Categories</p>
                            <p><span> 100 </span> Total Feature</p>
                        </div>
                        <div className='right-content'>
                        <Input
                            placeholder="Search for username, email, role, etc"
                            prefix={<Search className="site-form-item-icon" />}  
                        />
                        <Button type="primary" onClick={()=>this.addNewTast()}>
                        <div className='btn-wrapper'><Plus/> Add New</div> 
                        </Button>
                        </div>
                    </div>
                    <div className='content-wrapper'>
                        <div className='content-table-body'>
                            <Table 
                                columns={columns}  
                                dataSource={data} 
                                scroll={{ y: 500 }}
                                content= {content}
                            />
                        </div>
                    </div>
                </div>
                </Spin>
                <Modal className={editable ? "add-new-task editable" : "add-new-task"} title="Add New Feature" maskClosable={false} visible={isModalVisible} onCancel={this.handleCancel} footer={false}>
                    <Spin spinning={loaderDetail}>
                    <Form>
                        <div className="split-row">
                            <Form.Item label="Category">
                                <Select 
                                    placeholder={editable === true ? detailData.category : 'Please Select Feature'} 
                                    onChange={(value,)=>this.addNewData(value, "category")}>
                                    <Select.Option value="Settings">Setting</Select.Option>
                                    <Select.Option value="All Category">All Category</Select.Option>
                                </Select>
                            </Form.Item>
                            <Form.Item label="Feature">
                                <Select 
                                    placeholder={editable === true ? detailData.feature : "Please Select Feature"}
                                    onChange={(value,)=>this.addNewData(value, "feature")}>
                                    <Select.Option value="User Activity">User Activity</Select.Option>
                                    <Select.Option value="User Account">User Account</Select.Option>
                                </Select>
                            </Form.Item>
                        </div>
                        <div className="split-row">
                            <Form.Item label="Sub-Feature">
                                <Select 
                                    placeholder={editable === true ? detailData.subfeature : "Please Select Sub Feature"} 
                                    onChange={(value,)=>this.addNewData(value, "subfeature")}>
                                    <Select.Option value="Withdraw">Withdraw</Select.Option>
                                    <Select.Option value="Trading">Trading</Select.Option>
                                </Select>
                            </Form.Item>
                            <Form.Item label="Project Manager">
                                <Select 
                                    placeholder={editable === true ? detailData.projectmanager : "Please Select Project Manager"}
                                    onChange={(value,)=>this.addNewData(value, "projectmanager")}>
                                    <Select.Option value="Yusuf">Yusuf</Select.Option>
                                    <Select.Option value="Dickson">Dickson</Select.Option>
                                </Select>
                            </Form.Item>
                        </div>
                        <div className="split-row">
                            <Form.Item label="Partners">
                                <Select 
                                    placeholder={editable === true ? detailData.partner : "Please Select Partners"}
                                    onChange={(value,)=>this.addNewData(value, "partner")}>
                                    <Select.Option value="Faldo">Faldo</Select.Option>
                                    <Select.Option value="Yolan">Yolan</Select.Option>
                                </Select>
                            </Form.Item>
                            <Form.Item label="Figma Link" onChange={(value)=>this.addNewDataInput(value, "link")}>
                                <Input 
                                    value={newTask.link} 
                                    placeholder={editable === true ? detailData.link : "Please Input Figma Link"}/>
                            </Form.Item>
                        </div>
                        <div className="full-row">
                        <Form.Item label="Tags">
                                <Input 
                                    value={newTask.tags}
                                    placeholder={editable === true ? detailData.tags : "Please Input Tags"}/>
                        </Form.Item>
                        </div>
                        <div className="full-row">
                        <Form.Item label="Notes">
                                <Input 
                                    value={newTask.notes}
                                    placeholder={editable === true ? detailData.notes : "Please Input Notes"}/>
                            </Form.Item>
                        </div>
                    </Form>
                    </Spin>
                    <div className='footer'>
                        <Button className="cancel-btn" onClick={() => this.cancelModal()}>Cancel</Button>
                        {editable === false ? 
                        <Button className='add-btn' onClick={()=> this.addNewTask()}>Add</Button> :
                        <Button className='add-btn' onClick={()=> this.editDetailData()}>Edit</Button> 
                        }
                    </div>
                </Modal>

            </Fragment>
        )
    }
}

export default connect(
    state => ({
       AllCategoriesState : state.AllCategories,
       detailDataState : state.detailData,
    }),
    {getDataList, addNewTask, deleteDatalist, getDetailData, editDetailData}
 )(AllCategories)



