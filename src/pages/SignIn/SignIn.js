import React from 'react'
import { Input, Button, Switch } from 'antd';
import Stockbit from '../../asset/stockbit.svg'
// import StockbitBtn from '../../asset/stockbit-btn.svg'
import {
    Link
  } from "react-router-dom";

import './SignIn.css'


class SignIn extends React.Component{
    render(){
        return(
            <div className='sign-in-page'>
                <div className='header-page'>
                    <img src={Stockbit} alt=""/>
                    <p>Stockbibit Documentations</p>
                </div>
                <div className='content-page'>
                    <div className='card-wrapper'>
                        <div className='card-title'>
                            <p>Welcome</p>
                            <p>Everything you need to get your Stockbibit Documentations.</p>
                        </div>
                        <div className='card-form'>
                            <div className='input-wrapper'>
                                <p>Email / Username</p>
                                <Input
                                    placeholder="Your Email"
                                />
                            </div>
                            <div className='input-wrapper'>
                                <p>Password</p>
                                <Input
                                    placeholder="Password"
                                />
                            </div>
                            <div className='switch-wrapper'>
                                <Switch />
                                <p>Remember me</p>
                            </div>
                            <div className='button-wrapper'>
                                <Link to='/dashboard'>
                                <Button className='top-btn'>
                                    Sign In
                                </Button>
                                </Link>
                             
                                {/* <div className='stockbit-btn-wrapper'>
                                    <Button>
                                        <img src={StockbitBtn} alt=""/>
                                    </Button>
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>
                <div className='footer-page'>
                    <p>StockbibitDesign</p>
                    <p>© 2022 Product Design Team. All rights reserverd.</p>
                </div>
            </div>
        )
    }
}

export default SignIn
