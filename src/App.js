import { Layout, Menu, Dropdown } from 'antd';
import React from 'react';
import MenuIcon from './asset/menu.svg'
import {ReactComponent as AllIcon} from './asset/icon-all.svg'
import {ReactComponent as DownArrow} from './asset/down-arrow.svg'
import {ReactComponent as Login} from './asset/login.svg'
import {ReactComponent as OpeningRDN} from './asset/opening-rdn.svg'
import {ReactComponent as Setting} from './asset/settings.svg'
import {ReactComponent as Stream} from './asset/stream.svg'
import {ReactComponent as Watchlist} from './asset/watchlist.svg'
import { Provider } from 'react-redux';
import {store} from './redux-saga/store'
import Avatar from './asset/avatar.svg'
import AllCategories from './pages/AllCategories/AllCategories'
import SignIn from './pages/SignIn/SignIn'
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import './App.css'

const { Header, Sider, Content } = Layout;


class App extends React.Component {
  state = {
    collapsed: false,
    menuActive: 1,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  menuSwitch (params) {
    this.setState({
      menuActive: params
    })
  }


  
  render() {
    const {collapsed, menuActive} = this.state
    const menu = (
      <Menu>
        <Menu.Item key="0">
          <a href="https://www.antgroup.com">1st menu item</a>
        </Menu.Item>
        <Menu.Item key="1">
          <a href="https://www.aliyun.com">2nd menu item</a>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="3">3rd menu item</Menu.Item>
      </Menu>
    );
    return (
      <Provider store = {store}>
      <BrowserRouter>
      <Routes>
      <Route 
        path="/dashboard"
        element={<Layout className='web-docomentation-wrapper'>
        <Header className={collapsed === false ? 'web-documentation-header' : 'web-documentation-header collapse' }>
          <div className='header-wrapper'>
            <img src={Avatar} alt="ava"/>
            <Dropdown overlay={menu} trigger={['click']}>
              <a className="ant-dropdown-link" onClick={e => e.preventDefault()} href="/">
                Hello, <span>Yolan!</span> <DownArrow />
              </a>
            </Dropdown>
          </div>
          
        </Header>
        <Layout className='web-documentation-content-wrapper'>
        <Sider className={collapsed === false ? 'web-documentation-sider' : 'web-documentation-sider collapse'} trigger={null} collapsible collapsed={this.state.collapsed}>
          <Menu className='web-documentation-menu' theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <img className='menu-trigger' src={MenuIcon} alt="menu" onClick={this.toggle}/>
            <p className={collapsed === false ? 'categories-title' : 'categories-title hide'}>CATEGORIES</p>
            <Menu.Item key="1" icon={<AllIcon/>} onClick={()=>this.menuSwitch(1)}>
                All Categories 
            </Menu.Item>
            <Menu.Item key="2" icon={<Login />} onClick={()=>this.menuSwitch(2)}>
              Login and Register
            </Menu.Item>
            <Menu.Item key="3" icon={<OpeningRDN />} onClick={()=>this.menuSwitch(3)}>
              Opening RDN
            </Menu.Item>
            <Menu.Item key="4" icon={<Setting />} onClick={()=>this.menuSwitch(4)}>
              Settings
            </Menu.Item>
            <Menu.Item key="5" icon={<Watchlist />} onClick={()=>this.menuSwitch(5)}>
              Watchlist
            </Menu.Item>
            <Menu.Item key="6" icon={<Stream />} onClick={()=>this.menuSwitch(6)}>
                Stream
            </Menu.Item>
          </Menu>
        </Sider>
          <Content className={collapsed === false ? 'web-documentation-content' : 'web-documentation-content collapse'}>
            {
              menuActive === 1 ? <AllCategories /> :
              menuActive === 2 ? <div> content 2 </div> :
              <div>content 3 </div>
            }
          </Content>
        </Layout>
      </Layout>}
      />  
      <Route path='/' element={<SignIn />} />
      </Routes>
      </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
